class Code
  attr_reader :pegs

  PEGS = {
    "R" => "Red",
    "G" => "Green",
    "B" => "Blue",
    "Y" => "Yellow",
    "O" => "Orange",
    "P" => "Purple"
  }.freeze

  def self.random
    random_code = []
    4.times { random_code << PEGS.values.sample }
    self.new(random_code)
  end

  def self.parse(code_string)
    pegs = []
    code_string.each_char do |ch|
      raise "Not a legal color." if !PEGS.include?(ch.upcase)
      pegs << PEGS[ch.upcase]
    end

    self.new(pegs)
  end

  def initialize(code)
    @pegs = code
  end

  def [](idx)
    @pegs[idx]
  end

  def ==(code)
    return false if !code.is_a?(Code)
    pegs == code.pegs ? true : false
  end

  def exact_matches(guess)
    (0..3).map { |idx| self[idx] == guess[idx] ? 1 : 0 }.reduce(:+)
  end

  def near_matches(guess)
    near_count = 0
    code_hash, guess_hash = Hash.new(0), Hash.new(0)
    pegs.each { |color| code_hash[color] += 1 }
    guess.pegs.each do |color|
      guess_hash[color] += 1
      near_count += 1 if guess_hash[color] <= code_hash[color]
    end

    near_count - exact_matches(guess)
  end
end

class Game
  attr_reader :secret_code, :turn_count

  def initialize(code = Code.random)
    @turn_count = 0
    @secret_code = code
  end

  def play
    puts "Welcome to Mastermind! In this game, you will have to guess the colors of four pegs and their positions (in the form RGBY). You will have 10 guesses. After each guess, I will tell you which pegs were exat matches (correct color and location), as well as how many others were near matches (correct color, but incorrect location). Ready?"
    puts "The legal colors are:"
    puts "Red (R)\nGreen (G)\nBlue (B)\nYellow (Y)\nOrange (O)\nPurple (P)"

    while @turn_count < 10
      get_guess

      if @guess.pegs == @secret_code.pegs
        puts "Congratulations, you have guessed the code!"
        return nil
      else
        display_matches(@guess)
      end

      @turn_count
    end

    puts "You could not guess the code in 10 turns (#{@secret_code.pegs}). Game Over!"
  end

  def get_guess
    puts "You have #{10 - @turn_count} guesses remaining. "
    valid_guess = false
    until valid_guess
      print "Enter your next guess: "
      input = gets.chomp

      if input.length == 4 &&
        input.chars.all? { |ch| "RGBYOPrgbyop".include?(ch) }
        @guess = Code.parse(input)
        valid_guess = true
      else
        puts "Invalid guess.\nYour guess must be 4 characters long and only use the letters R, G, B, Y, O, and P."
      end
    end

    @turn_count += 1
    @guess
  end

  def display_matches(guess)
    puts "You have #{@secret_code.exact_matches(guess)} exact matches and #{@secret_code.near_matches(guess)} near matches."
  end
end

if $PROGRAM_NAME == __FILE__
  Game.new.play
end
